package com.geektechnique.customswagger.repositories;

import com.geektechnique.customswagger.domain.Vendor;
import org.springframework.data.jpa.repository.JpaRepository;

public interface VendorRepository extends JpaRepository<Vendor, Long> {
}
