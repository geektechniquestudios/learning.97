package com.geektechnique.customswagger.repositories;

import com.geektechnique.customswagger.domain.Customer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CustomerRepository extends JpaRepository<Customer, Long> {
}
