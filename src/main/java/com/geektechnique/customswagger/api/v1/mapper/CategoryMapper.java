package com.geektechnique.customswagger.api.v1.mapper;

import org.mapstruct.Mapper;
import com.geektechnique.customswagger.api.v1.model.CategoryDTO;
import com.geektechnique.customswagger.domain.Category;
import org.mapstruct.factory.Mappers;

@Mapper
public interface CategoryMapper {

    CategoryMapper INSTANCE = Mappers.getMapper(CategoryMapper.class);


    CategoryDTO categoryToCategoryDTO(Category category);
}
